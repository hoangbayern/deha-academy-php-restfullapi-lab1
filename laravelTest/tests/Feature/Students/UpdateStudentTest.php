<?php

namespace Tests\Feature\Students;

use App\Models\Student;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class UpdateStudentTest extends TestCase
{
    use WithFaker;
    /** @test  */
    public function user_can_update_student_if_student_exists_and_data_is_valid(){
        $student = Student::factory()->create();
        $dataCreate = [
            'name' => $this->faker->name,
            'age' => $this->faker->randomNumber(2),
            'address' => $this->faker->address
        ];
        $response = $this->putJson(route('students.update',$student->id),$dataCreate);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) =>
               $json->has('message')
                    ->has('data',fn(AssertableJson $json) =>
                          $json->where('name',$dataCreate['name'])
                          ->etc()
                    )->etc()
        );
        $this->assertDatabaseHas('students',[
           'name' => $dataCreate['name'],
           'age' => $dataCreate['age'],
           'address' => $dataCreate['address']
        ]);
    }
    /** @test  */
    public function user_can_not_update_student_if_student_exists_and_name_is_null(){
        $student = Student::factory()->create();
        $dataUpdate = [
            'name' => '',
            'age' => $this->faker->randomNumber(2),
            'address' => $this->faker->address
        ];
        $response = $this->putJson(route('students.update',$student->id),$dataUpdate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors',fn(AssertableJson $json) =>
        $json->has('name')
            ->etc()
        )

        );
    }
    /** @test  */
    public function user_can_not_update_student_if_student_exists_and_age_is_null(){
        $student = Student::factory()->create();
        $dataUpdate = [
            'name' => $this->faker->name,
            'age' => '',
            'address' => $this->faker->address
        ];
        $response = $this->putJson(route('students.update',$student->id),$dataUpdate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors',fn(AssertableJson $json) =>
        $json->has('age')
            ->etc()
        )

        );
    }
    /** @test  */
    public function user_can_not_update_student_if_student_exists_and_address_is_null(){
        $student = Student::factory()->create();
        $dataUpdate = [
            'name' => $this->faker->name,
            'age' => $this->faker->randomNumber(2),
            'address' => ''
        ];
        $response = $this->putJson(route('students.update',$student->id),$dataUpdate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors',fn(AssertableJson $json) =>
        $json->has('address')
            ->etc()
        )

        );
    }
    /** @test  */
    public function user_can_not_update_student_if_student_exists_and_nameAge_is_null(){
        $student = Student::factory()->create();
        $dataUpdate = [
            'name' => '',
            'age' => '',
            'address' => $this->faker->address
        ];
        $response = $this->putJson(route('students.update',$student->id),$dataUpdate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors',fn(AssertableJson $json) =>
        $json->has('name')
            ->has('age')
            ->etc()
        )

        );
    }
    /** @test  */
    public function user_can_not_update_student_if_student_exists_and_ageAddress_is_null(){
        $student = Student::factory()->create();
        $dataUpdate = [
            'name' => $this->faker->name(),
            'age' => '',
            'address' => ''
        ];
        $response = $this->putJson(route('students.update',$student->id),$dataUpdate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors',fn(AssertableJson $json) =>
        $json->has('age')
            ->has('address')
            ->etc()
        )

        );
    }
    /** @test  */
    public function user_can_not_update_student_if_student_exists_and_nameAddress_is_null(){
        $student = Student::factory()->create();
        $dataUpdate = [
            'name' => '',
            'age' => $this->faker->randomNumber(2),
            'address' => ''
        ];
        $response = $this->putJson(route('students.update',$student->id),$dataUpdate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors',fn(AssertableJson $json) =>
        $json->has('name')
            ->has('address')
            ->etc()
        )

        );
    }
    /** @test  */
    public function user_can_not_update_student_if_student_exists_and_data_is_null(){
        $student = Student::factory()->create();
        $dataUpdate = [
            'name' => '',
            'age' => '',
            'address' => ''
        ];
        $response = $this->putJson(route('students.update',$student->id),$dataUpdate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors',fn(AssertableJson $json) =>
        $json->has('name')
            ->has('age')
            ->has('address')
            ->etc()
        )
        );
    }
    /** @test  */
    public function user_can_not_update_student_if_student_not_exists_and_data_is_valid(){
        $studentId = -1;
        $dataUpdate = [
            'name' => $this->faker->name,
            'age' => $this->faker->randomNumber(2),
            'address' => $this->faker->address
        ];
        $response = $this->putJson(route('students.update',$studentId),$dataUpdate);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
