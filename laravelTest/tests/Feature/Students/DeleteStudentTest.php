<?php

namespace Tests\Feature\Students;

use App\Models\Student;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class DeleteStudentTest extends TestCase
{
    /** @test  */
    public function user_can_delete_student_if_student_exists(){
        $student = Student::factory()->create();
        $studentCountBeforeDelete = Student::count();
        $response = $this->deleteJson(route('students.destroy',$student->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('data',fn(AssertableJson $json) =>
        $json->where('name',$student->name)
            ->etc()
        )->etc()
        );
        $studentCountAfterDelete = Student::count();
        $this->assertEquals($studentCountBeforeDelete-1,$studentCountAfterDelete);
    }
    /** @test  */
    public function user_can_not_delete_student_if_student_not_exists(){
        $studentId = -1;
        $response = $this->deleteJson(route('students.destroy',$studentId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
