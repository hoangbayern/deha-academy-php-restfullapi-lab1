<?php

namespace Tests\Feature\Students;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class CreateStudentTest extends TestCase
{
    use WithFaker;
    /** @test  */
    public function user_can_create_student_if_data_is_valid(){
        $dataCreate = [
             'name' => $this->faker->name,
             'age' => $this->faker->randomNumber(2),
             'address' => $this->faker->address
        ];
        $response = $this->postJson(route('students.store',$dataCreate));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) =>
                $json->has('message')
                     ->has('data',fn(AssertableJson $json) =>
                            $json->where('name',$dataCreate['name'])
                            ->etc()
                     )->etc()
        );
        $this->assertDatabaseHas('students',[
            'name' => $dataCreate['name'],
            'age' => $dataCreate['age'],
            'address' => $dataCreate['address']
        ]);
    }
    /** @test  */
    public function user_can_not_create_student_if_name_is_null(){
        $dataCreate = [
            'name' => '',
            'age' => $this->faker->randomNumber(2),
            'address' => $this->faker->address
        ];
        $response = $this->postJson(route('students.store',$dataCreate));
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors' ,fn(AssertableJson $json) =>
                 $json->has('name')
                  ->etc()
        )
        );
    }
    /** @test  */
    public function user_can_not_create_student_if_age_is_null(){
        $dataCreate = [
            'name' => $this->faker->name,
            'age' => '',
            'address' => $this->faker->address
        ];
        $response = $this->postJson(route('students.store',$dataCreate));
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors' ,fn(AssertableJson $json) =>
        $json->has('age')
            ->etc()
        )
        );
    }
    /** @test  */
    public function user_can_not_create_student_if_address_is_null(){
        $dataCreate = [
            'name' => $this->faker->name,
            'age' => $this->faker->randomNumber(2),
            'address' => ''
        ];
        $response = $this->postJson(route('students.store',$dataCreate));
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors' ,fn(AssertableJson $json) =>
        $json->has('address')
            ->etc()
        )
        );
    }
    /** @test  */
    public function user_can_not_create_student_if_nameAge_is_null(){
        $dataCreate = [
            'name' => '',
            'age' => '',
            'address' => $this->faker->address
        ];
        $response = $this->postJson(route('students.store',$dataCreate));
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors' ,fn(AssertableJson $json) =>
        $json->has('name')
             ->has('age')
            ->etc()
        )
        );
    }
    /** @test  */
    public function user_can_not_create_student_if_ageAddress_is_null(){
        $dataCreate = [
            'name' => $this->faker->name,
            'age' => '',
            'address' => ''
        ];
        $response = $this->postJson(route('students.store',$dataCreate));
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors' ,fn(AssertableJson $json) =>
        $json->has('age')
            ->has('address')
            ->etc()
        )
        );
    }
    /** @test  */
    public function user_can_not_create_student_if_nameAddress_is_null(){
        $dataCreate = [
            'name' => '',
            'age' => $this->faker->randomNumber(2),
            'address' => ''
        ];
        $response = $this->postJson(route('students.store',$dataCreate));
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors' ,fn(AssertableJson $json) =>
        $json->has('name')
            ->has('address')
            ->etc()
        )
        );
    }
    /** @test  */
    public function user_can_not_create_student_if_data_is_null(){
        $dataCreate = [
            'name' => '',
            'age' => '',
            'address' => ''
        ];
        $response = $this->postJson(route('students.store',$dataCreate));
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors' ,fn(AssertableJson $json) =>
        $json->has('name')
            ->has('age')
            ->has('address')
        )
        );
    }
}
