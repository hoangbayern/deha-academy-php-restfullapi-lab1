<?php

namespace Tests\Feature\Students;

use App\Models\Student;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class GetStudentTest extends TestCase
{
    /** @test  */
    public function user_can_get_student_if_student_exists(){
        $student = Student::factory()->create();
        $response = $this->getJson(route('students.show',$student->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) =>
                 $json->has('data',fn(AssertableJson $json) =>
                            $json->where('name',$student->name)
                                 ->etc()
                 )
                      ->has('message')
        );
    }
    /** @test  */
    public function user_can_not_get_student_if_student_not_exists(){
        $studentID = -1;
        $reponse = $this->getJson(route('students.show',$studentID));
        $reponse->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
