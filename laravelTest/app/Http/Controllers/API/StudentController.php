<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreStudentRequest;
use App\Http\Requests\UpdateStudentRequest;
use App\Http\Resources\StudentCollection;
use App\Http\Resources\StudentResource;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class StudentController extends Controller
{
    protected $student;

    /**
     * @param $student
     */
    public function __construct(Student $student)
    {
        $this->student = $student;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = $this->student->paginate(5);
        $studentsCollection = new StudentCollection($students);
        return $this->sentSuccessResponse($studentsCollection,'success',Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreStudentRequest $request)
    {
        $dataCreate = $request->all();
        $student = $this->student->create($dataCreate);

        $studentResource = new StudentResource($student);
        return $this->sentSuccessResponse($studentResource,'add success',Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student = $this->student->findOrFail($id);

        $studentResource = new StudentResource($student);
        return $this->sentSuccessResponse($studentResource,'success',Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateStudentRequest $request, $id)
    {
        $student = $this->student->findOrFail($id);
        $dataCreate = $request->all();
        $student->update($dataCreate);

        $studentResource = new StudentResource($student);
        return $this->sentSuccessResponse($studentResource,'update success',Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = $this->student->findOrFail($id);
        $student->delete();

        $studentResource = new StudentResource($student);
        return $this->sentSuccessResponse($studentResource,'delete success',Response::HTTP_OK);
    }
}
